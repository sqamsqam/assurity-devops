# Azure Resource Manager Provider
provider "azurerm" {
  features {}
}

# Resource Group
resource "azurerm_resource_group" "main" {
  name     = format("%s-resources", var.prefix)
  location = var.location
}

# SSH key generation to authenticate with SSH server
resource "tls_private_key" "main" {
  algorithm = "RSA"
  rsa_bits  = "2048"
}
