resource "azurerm_linux_virtual_machine" "main" {
  name                = format("%s-vm", var.prefix)
  resource_group_name = azurerm_resource_group.main.name
  location            = azurerm_resource_group.main.location
  size                = var.vm_size
  admin_username      = var.admin_username
  network_interface_ids = [
    azurerm_network_interface.main.id,
  ]

  # As per the azurerm module documentation this should not be need and should default to true
  # The only reason it seems to include this is when using password authentication in which case this would be set to false
  # Setting this to true for the sake of being explicit
  # https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/linux_virtual_machine#disable_password_authentication
  disable_password_authentication = true

  admin_ssh_key {
    username   = var.admin_username
    public_key = tls_private_key.main.public_key_openssh
  }

  custom_data = base64encode(data.template_file.cloud-init.rendered)

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  # Connect to the VM using SSH and execute the same command used by pam_login
  provisioner "remote-exec" {
    inline = [
      "sudo run-parts /etc/update-motd.d",
    ]

    connection {
      agent       = false
      timeout     = "5m"
      host        = self.public_ip_address
      user        = self.admin_username
      private_key = tls_private_key.main.private_key_pem
    }
  }
}

data "template_file" "cloud-init" {
  template = file("user-data.sh")
}