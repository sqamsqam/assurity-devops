variable "prefix" {
  description = "Resource Prefix"
  default     = "assurity"
}

variable "location" {
  description = "Resource Location (region)"
  default     = "Australia East"
}

variable "vm_size" {
  description = "Instance Size/Type"
  default     = "Standard_B1ls"
}

variable "admin_username" {
  description = "Username of the Admin User"
  default     = "terraform"
}