#! /bin/bash
echo "Private Key (pem):"
terraform show -json terraform.tfstate | jq -r '.values.root_module.resources[] | select(.address=="tls_private_key.main") | .values.private_key_pem'
echo "Public Key (pem):"
terraform show -json terraform.tfstate | jq -r '.values.root_module.resources[] | select(.address=="tls_private_key.main") | .values.public_key_pem'
echo "Public Key (openSSH):"
terraform show -json terraform.tfstate | jq -r '.values.root_module.resources[] | select(.address=="tls_private_key.main") | .values.public_key_openssh'

echo "Admin Username:"
terraform show -json terraform.tfstate | jq -r '.values.root_module.resources[] | select(.address=="azurerm_linux_virtual_machine.main") | .values.admin_username'

echo "Public IP Address"
terraform show -json terraform.tfstate | jq -r '.values.root_module.resources[] | select(.address=="azurerm_linux_virtual_machine.main") | .values.public_ip_address'
