# assurity-devops

Assurity DevOps Technical Test

## Acceptance Criteria:
Using automation tooling of your choice (with the exception of Vagrant):

* provision an Ubuntu 16.04 server
* carry out any configuration steps needed to allow login over ssh, securely and disable password based login
* Setting up the firewall to only allow ingress on the ssh port and only to allow password-less (certificate) based login
* then display the MOTD including the text "Hello Assurity DevOps”.

## Notes
I have set up terraform to generate its own ssh key which can be pulled from the state using the `get-vm-details.sh` script. This script assumes the terraform.tfstate file is in the same directory

There is a `remote-exec` provisioner set up for the VM which connects via ssh to trigger the same command run by pam_login which happens when someone logs in via SSH so you will be able to verify the "Hello Assurity DevOps" motd is present by looking at the output from `terraform apply`. if you want to use ssh to verify manually please pull the required details from the `get-vm-details.sh` script.
